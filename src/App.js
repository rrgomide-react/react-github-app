import React, { Component } from 'react';

import { AppContent } from './components/AppContent';

const ENTER = 13;

class App extends Component {
  constructor() {
    super();

    this.state = {
      isFetchingData: false,
      repos: [],
      starred: [],
      userInfo: null,
    };
  }

  _handleSearch = async event => {
    /**
     * Obtendo o código da tecla recém digitada
     * event.which || event.keyCode é um fallback
     * para navegadores mais antigos, que só fornecem
     * event.keyCode. Portanto, na ausência de event.which
     * obtemos event.keyCode.
     *
     * Fonte: https://www.w3schools.com/jsref/event_key_keycode.asp
     */
    const keyCode = event.which || event.keyCode;

    /**
     * Somente processamos se a tecla for ENTER
     */
    if (keyCode !== ENTER) return;

    try {
      /**
       * Indicando que o app irá buscar
       * dados na API e, portanto,
       * está em um estado de carga de dados
       */
      this.setState({ isFetchingData: true });

      const objGithub = await this._requestGithubUser(
        event.target.value.trim(),
      );

      this.setState({
        userInfo: {
          photo: objGithub.avatar_url,
          followers: objGithub.followers,
          following: objGithub.following,
          login: objGithub.login,
          name: objGithub.name,
          repos: objGithub.public_repos,
        },
        repos: [],
        starred: [],
        isFetchingData: false,
      });
    } catch (error) {
      console.error(`Erro na busca de usuários: ${error}`);

      this.setState(
        {
          userInfo: null,
          repos: [],
          starred: [],
          isFetchingData: false,
        },
        /**
         * É importante também reabilitar
         * o campo em caso de erro
         */
        //() => (target.disabled = false),
      );
    }
  };

  _requestGithubUser = async login => {
    const res = await fetch(`https://api.github.com/users/${login}`);
    if (res.status === 200) {
      const json = await res.json();
      return json;
    } else {
      console.warn(`Usuário ${login} não encontrado`);
      return undefined;
    }
  };

  _handleProjects = async (login, projectType) => {
    /**
     * Montando a url dinamicamente
     * e executando a requisição
     */
    const url = `https://api.github.com/users/${login}/${projectType}`;
    const res = await fetch(url);

    if (res.status !== 200) {
      console.warn(`Falha ao acessar url: ${url}`);
      return undefined;
    }

    /**
     * Se chegou aqui, tudo
     * ok para processar os dados
     */
    const json = await res.json();

    const projects = json.map(item => ({
      name: item.name,
      url: item.html_url,
    }));

    this.setState({
      /**
       * A notação de colchetes converte a string
       * do tipo de repositório ('repos', 'starred')
       * na propriedade correspondente de state
       */
      [projectType]: projects,
    });
  };

  _handleRepos = async login => {
    return await this._handleProjects(login, 'repos');
  };

  _handleStarred = async login => {
    return await this._handleProjects(login, 'starred');
  };

  render() {
    return (
      <AppContent
        handleRepos={() => this._handleRepos(this.state.userInfo.login)}
        handleSearch={e => this._handleSearch(e)}
        handleStarred={() => this._handleStarred(this.state.userInfo.login)}
        isFetchingData={this.state.isFetchingData}
        repos={this.state.repos}
        starred={this.state.starred}
        userInfo={this.state.userInfo}
      />
    );
  }
}

export default App;
