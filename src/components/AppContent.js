import React from 'react';
import PropTypes from 'prop-types';

import { Search } from './Search';
import { UserInfo } from './UserInfo';
import { Repos } from './Repos';
import { Navbar } from './Navbar';
import { Carregando } from './Carregando';

//export const AppContent = props => (
export const AppContent = ({
  handleRepos,
  handleSearch,
  handleStarred,
  isFetchingData,
  repos,
  starred,
  userInfo,
}) => {
  return (
    <div className="app">
      <Search disabled={isFetchingData} handleSearch={handleSearch} />
      {isFetchingData ? (
        <Carregando />
      ) : (
        _renderPage(userInfo, handleRepos, handleStarred, repos, starred)
      )}
    </div>
  );
};

const _renderPage = (userInfo, handleRepos, handleStarred, repos, starred) => {
  return (
    <div>
      <Navbar
        userInfo={userInfo}
        handleRepos={handleRepos}
        handleStarred={handleStarred}
      />
      {!!userInfo && <UserInfo userInfo={userInfo} />}
      {!!repos.length && (
        <Repos className="repos" title="Repositórios" repos={repos} />
      )}
      {!!starred.length && (
        <Repos className="repos" title="Favoritos" repos={starred} />
      )}
    </div>
  );
};

AppContent.defaultProps = {
  handleSearch: null,
  repos: [],
  starred: [],
  userInfo: {},
};

AppContent.propTypes = {
  handleSearch: PropTypes.func.isRequired,
  repos: PropTypes.array.isRequired,
  starred: PropTypes.array.isRequired,
  userInfo: PropTypes.object,
};
