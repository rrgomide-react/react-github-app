import React from 'react';

export const Actions = ({ handleRepos, handleStarred }) => (
  <div className="actions">
    <label>
      <input
        type="button"
        name="actions"
        value="Repositórios"
        onClick={handleRepos}
      />
    </label>
    <label>
      <input
        type="button"
        name="actions"
        value="Favoritos"
        onClick={handleStarred}
      />
    </label>
  </div>
);
