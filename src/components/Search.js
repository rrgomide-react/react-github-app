import React from 'react';
import PropTypes from 'prop-types';

export const Search = ({ disabled, handleSearch }) => (
  <div className="search">
    <input
      disabled={disabled}
      type="search"
      placeholder="Informe o nome do usuário no Github"
      onKeyUp={handleSearch}
    />
  </div>
);

Search.propTypes = {
  handleSearch: PropTypes.func.isRequired,
};
