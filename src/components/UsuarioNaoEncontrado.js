import React from 'react';

const pStyle = {
  fontWeight: 'bold',
  fontSize: '1.1em',
  textAlign: 'center',
};

const divStyle = {
  paddingTop: '20px',
};

export const UsuarioNaoEncontrado = () => (
  <div style={divStyle}>
    <p style={pStyle}>Nenhum usuário encontrado!</p>
  </div>
);
