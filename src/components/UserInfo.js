import React from 'react';
import PropTypes from 'prop-types';

const avatarStyle = {
  width: '200px',
  borderRadius: '100px',
  marginRight: '10px',
};

const fullNameLinkStyle = {
  textDecoration: 'none',
  fontWeight: 'bold',
  color: '#333',
};

const userInfoStyle = {
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
};

export const UserInfo = ({ userInfo }) => {
  console.log(userInfo);
  return !!userInfo.name ? _getUserRender(userInfo) : null;
};

const _getUserRender = userInfo => {
  return (
    <div style={userInfoStyle}>
      <img
        style={avatarStyle}
        src={userInfo.photo}
        alt={userInfo.name}
        title={userInfo.name}
      />

      <div className="user-data">
        <h2>
          <a
            style={fullNameLinkStyle}
            href={`https://github.com/${userInfo.login}`}
            target="_blank"
            rel="noopener noreferrer">
            {`${userInfo.name} (${userInfo.login})`}
          </a>
        </h2>

        <ul className="repos-info">
          <li>Repositórios: {userInfo.repos}</li>
          <li>Seguidores: {userInfo.followers}</li>
          <li>Seguindo: {userInfo.following}</li>
        </ul>
      </div>
    </div>
  );
};

UserInfo.defaultProps = {
  followers: 0,
  following: 0,
  login: 'nao_encontrado',
  name: 'Não encontrado',
  photo: '',
  repos: 0,
};

UserInfo.propTypes = {
  userInfo: PropTypes.shape({
    followers: PropTypes.number.isRequired,
    following: PropTypes.number.isRequired,
    login: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    photo: PropTypes.string.isRequired,
    repos: PropTypes.number.isRequired,
  }),
};
