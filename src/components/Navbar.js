import React from 'react';

import { Actions } from './Actions';
import { UsuarioNaoEncontrado } from './UsuarioNaoEncontrado';

const divStyle = {
  marginBottom: '20px',
};

export const Navbar = ({ userInfo, handleRepos, handleStarred }) => (
  <div style={divStyle}>
    {!!userInfo ? (
      <Actions handleRepos={handleRepos} handleStarred={handleStarred} />
    ) : (
      <UsuarioNaoEncontrado />
    )}
  </div>
);
