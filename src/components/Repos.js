import React from 'react';
import PropTypes from 'prop-types';

export const Repos = props => (
  <div className={props.className}>
    <h2>{props.title}</h2>
    <ul>
      {props.repos.map(repo => (
        <li key={repo.url}>
          <a href={repo.url} target="_blank">{repo.name}</a>
        </li>
      ))}
    </ul>
  </div>
);

Repos.defaultProps = {
  className: '',
  title: 'Título indefinido'
}

Repos.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  repos: PropTypes.array,
}
