import React from 'react';
import ReactSpinner from 'react-spinjs-new';

export const Carregando = () => {
  return <ReactSpinner />;
};
